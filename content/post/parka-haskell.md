---
title: "Parka w Haskellu"
date: 2018-06-13T01:39:37+02:01
draft: false
---
Postanowiłem dziś zacząć wrzucać do "szuflady" swoje wypociny na tematy zupełnie losowe ... Losowe, aczkolwiek kalające się w okolicach IT.

Lubiany przeze mnie [Marek Penszko](https://pl.wikipedia.org/wiki/Marek_Penszko), zapostował niedawno swoją wariację [zagadki Platona i Sokratesa](https://penszko.blog.polityka.pl/2018/06/06/parka/). W niniejszym, dziewiczym poście pokażę jak całkiem szybko poradzić sobię z nią z użyciem Haskella.

Drogi czytelniku, jeśliś tego nie zrobił, przeczytaj treść łamigłówki! 

W tym miejscu zakładam, że wiesz o czym czytasz - do rozwiązania więc:

W łamigłówce stoi "(...) tylko sumę, która jest mniejsza niż sto. (...)". Szukamy więc liczb z przedziału [2,100]:

```hs
dziedzina_min = 2
dziedzina_max = 100 
dziedzina = [dziedzina_min .. dziedzina_max]
```

Dalej czytamy, że M nie wie, jakie to liczby. Znaczy to tyle, że M wziął swoją liczbę i okazało się, że niejednoznacznie rozkłada się ona na iloczyn liczb. Zapiszmy to w Haskellu:

```hs
fakt_01 (a,b) = not $ singleton $ rozklad_na_iloczyn $ a * b
```

Pomocnicze funkcje singleton i rozklad_na_iloczyn definiowane są jak następuje:

```hs
singleton :: [a] -> Bool
singleton [_] = True
singleton _ = False

rozklad_na_iloczyny :: [[(Int,Int)]]
rozklad_na_iloczyny = map rozloz [0 ..] where
 rozloz n = [(a,b) | a <- dziedzina, b <- [a .. dziedzina_max], a * b == n]

rozklad_na_iloczyn :: Int -> [(Int,Int)]
rozklad_na_iloczyn = (!!) rozklad_na_iloczyny

```

Chwilę potem czytamy, że D nie wie o jakie liczby chodzi oraz, że wiedział, że M również nie będzie wiedział o jakich liczbach jest mowa. Oznacza to, że D rozłożył swoją sumę na wszystkie możliwe sposoby i okazało się, że każdy iloczyn tych rozkładów nie daje się jednoznacznie rozłożyć na iloczyn dwóch czynników. Daje to następujace fakty:

```hs
fakt_02 (a,b) = all fakt_01 $ rozklad_na_sume $ a + b
fakt_03 (a,b) = not $ singleton $ rozklad_na_sume $ a + b
```

W tym wypadku rozklad_na_sume definiujemy jako:
```hs
rozklad_na_sumy :: [[(Int,Int)]]
rozklad_na_sumy = map rozloz [0..] where
 rozloz n = [(a,b) | a <- dziedzina, b <- [a .. dziedzina_max], a + b == n]

rozklad_na_sume :: Int -> [(Int,Int)]
rozklad_na_sume = (!!) rozklad_na_sumy
```

Usłyszawszy to M mówi, że wie jakie liczby tworzą jego iloczyn. Daje to kolejny fakt:
```hs
fakt_04 (a,b) = singleton $ filter fakt_03 $ filter fakt_02 $ rozklad_na_iloczyn $ a * b
```

D nie zostaje dłużny i również twierdzi, że wie o jakie liczby chodzi:
```hs
fakt_05 (a,b) = singleton $ filter fakt_04 $ rozklad_na_sume $ a + b
```

Rozwiązaniem łamigłówki będą takie liczby, które spełniaja wszystkie 5 faktów:
```hs
rozwiazanie = [(a,b) | a <- dziedzina, b <- [a .. dziedzina_max], all ($ (a,b)) [fakt_01, fakt_02, fakt_03, fakt_04, fakt_05]]
```

Haskell krótko "myśląc" [odpowiada](https://ideone.com/j508QD):
```
[(4,13)]
```

Skrócona wersja programu w Haskellu do przeklejenia:
```hs
rni = (!!) $ map f [0..] where f n = [(a,b) | a <- [2..100], b <- [a..100], a * b == n]
rns = (!!) $ map f [0..] where f n = [(a,b) | a <- [2..100], b <- [a..100], a + b == n]

si [_] = True
si _ = False

f1 (a,b) = not $ si $ rni $ a * b
f2 (a,b) = all f1 $ rns $ a + b
f3 (a,b) = not $ si $ rns $ a + b
f4 (a,b) = si $ filter f2 $ rni $ a * b
f5 (a,b) = si $ filter f4 $ rns $ a + b

sol = [(a,b) | a <- [2..100], b <- [a..100], all ($ (a,b)) [f1, f2, f3, f4, f5]]
```

PS. I to wszystko nie wspominając nawet o Goldbachu xD
